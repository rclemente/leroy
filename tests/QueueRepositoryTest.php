<?php

use App\Repositories\QueueRepository;
use App\Models\Queue;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class QueueRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        $queue = new Queue();
        $queue->id = 9999999;
        $queue->status = 0;
        $queue->save();
    }

    public function testIfQueueWasCreated()
    {
        $queue = array(
            'id' => '999997',
            'status' => 0
        );
        $this->assertNotNull(QueueRepository::insert($queue));
    }

    public function testIfQueueWasUpdated()
    {

        $this->assertTrue(QueueRepository::edit('9999999', 3));
    }
}
