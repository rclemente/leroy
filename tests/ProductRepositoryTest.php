<?php

use App\Repositories\ProductRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use League\Flysystem\Exception;

class ProductRepositoryTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        App\Models\Product::create([
            'lm' => '9999999',
            'name' => 'ProductFake',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 32.50
        ]);
        App\Models\Product::create([
            'lm' => '9999998',
            'name' => 'ProductFake',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 32.50
        ]);
    }

    public function testIfProductWasCreated()
    {
        $product = array(
            'lm' => '9999992',
            'name' => 'Product1',
            'category' => '1',
            'free_shipping' => 1,
            'description'  => 'Product 1',
            'price' => 32.50
        );
        $this->assertTrue(ProductRepository::insert($product));
    }

    public function testIfProductWasDeleted()
    {
        $this->assertTrue(ProductRepository::exclude(9999999));
    }

    public function testIfProductWasNotFoundOnDeleteMethod()
    {
        $this->assertFalse(ProductRepository::exclude(9999991));
    }

    public function testIfProductWasUpdated()
    {
        $product = array(
            'lm' => '9999999',
            'name' => 'ProductFakeUpdated',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake Updated',
            'price' => 32.50
        );
        $this->assertTrue(ProductRepository::edit(9999999, $product));
    }

    public function testIfProductWasNotFoundOnUpdateMethod()
    {
        $product = array(
            'lm' => '9999999',
            'name' => 'ProductFakeUpdated',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake Updated',
            'price' => 32.50
        );
        $this->assertFalse(ProductRepository::edit(9999991, $product));
    }
    public function testIfProductsWasRead()
    {
        $this->assertNotFalse(ProductRepository::read());
    }

    public function testIfSingleProductWasFound()
    {
        $this->assertNotFalse(ProductRepository::read('9999999'));
    }

    //Process
    public function testFileProcessWithIncorrectProductLayout()
    {
        $this->setExpectedException(Exception::class, 'Produtos fora do layout');
        ProductRepository::process('incorrect_layout.xlsx', 1, 'tests\\files\\');
    }

    public function testFileProcessWithoutCategory()
    {
        $this->setExpectedException(Exception::class, 'Codigo de categoria invalido');
        ProductRepository::process('without_category.xlsx', 1, 'tests\\files\\');
    }

    public function testFileProcessWithoutProducts()
    {
        $this->setExpectedException(Exception::class, 'Produtos nao encontrados');
        ProductRepository::process('without_products.xlsx', 1, 'tests\\files\\');
    }

    /**
    * @doesNotPerformAssertions
    */
    public function testFileProcessCorrectLayout()
    {
        ProductRepository::process('correct_layout.xlsx', 1, 'tests\\files\\');
    }
}
