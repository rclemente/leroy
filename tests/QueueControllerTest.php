<?php

use App\Models\Queue;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class QueueControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();

        $queue = new Queue();
        $queue->id = 99999123;
        $queue->status = 0;
        $queue->save();
    }

    public function testReadQueue()
    {
        $this->get('api/queue/99999123');

        $this->assertResponseStatus(200);

        $this->seeJsonStructure([
            "id",
            "status",
            "comments",
            "created_at",
            "updated_at",
            "statusDescription",
        ]);
    }

    public function testReadInvalidQueue()
    {
        $this->get('api/queue/292923222212312');

        $this->assertResponseStatus(404);
    }
}
