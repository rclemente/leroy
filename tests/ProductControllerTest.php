<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;

class ProductControllerTest extends TestCase
{
    use DatabaseTransactions;


    protected function setUp()
    {
        parent::setUp();
        App\Models\Product::create([
            'lm' => '9999999',
            'name' => 'ProductFake',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 32.50
        ]);
        App\Models\Product::create([
            'lm' => '9999998',
            'name' => 'ProductFake',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 32.50
        ]);
    }

    public function testReadSingleProduct()
    {
        $this->get('api/products/9999999');

        $this->assertResponseStatus(200);

        $this->seeJsonStructure([
            "lm",
            "category",
            "name",
            "free_shipping",
            "description",
            "price",
            "deleted_at",
            "created_at",
            "updated_at"
        ]);
    }

    public function testIfFailOnProductNotFound()
    {
        $this->get('api/products/9999991');

        $this->assertResponseStatus(404);
    }

    public function testReadAllProduct()
    {
        $this->get('api/products');

        $this->assertResponseStatus(200);

        $this->seeJsonStructure([
            '*' => [
                "lm",
                "category",
                "name",
                "free_shipping",
                "description",
                "price",
                "deleted_at",
                "created_at",
                "updated_at"
            ]
        ]);
    }

    public function testIfProductHasBeenUpdated()
    {
        $postData = array(
            'lm' => '9999998',
            'name' => 'ProductFake Updated',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 35.50
        );

        $this->call('PUT', 'api/products/9999998', $postData);

        $this->assertResponseStatus(200);
    }

    public function testIfProductToBeUpdatedIsNotFound()
    {
        $postData = array(
            'lm' => '9999991',
            'name' => 'ProductFake Updated',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 35.50
        );

        $this->call('PUT', 'api/products/9999991', $postData);

        $this->assertResponseStatus(403);
    }

    public function testIfProductToBeUpdatedIsNotValid()
    {
        $postData = array(
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 35.50
        );

        $this->call('PUT', 'api/products/9999998', $postData);

        $this->assertSessionHasErrors();
    }

    public function testIfProductCanBeDeleted()
    {
        $this->call('DELETE', 'api/products/9999998');

        $this->assertResponseStatus(200);
    }

    public function testIfProductToDeletedIsNotFound()
    {
        $this->call('DELETE', 'api/products/9999991');

        $this->assertResponseStatus(403);
    }



    public function testIfProductFileUpload()
    {
        $stub = __DIR__. '/files/correct_layout.xlsx';

        $name = str_random(8).'.xlsx';
        $path = sys_get_temp_dir().'/'.$name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), mime_content_type($path), null, true);
        $this->call('POST', 'api/products', [], [], ['products' => $file], []);

        $this->assertResponseOk();

        @unlink(public_path('files') . $name);
    }



    public function testIfProductFileUploadInvalidFile()
    {
        $stub = __DIR__. '/files/incorrect_type.pdf';

        $name = str_random(8).'.pdf';
        $path = sys_get_temp_dir().'/'.$name;

        copy($stub, $path);

        $file = new UploadedFile($path, $name, filesize($path), mime_content_type($path), null, true);
        $this->call('POST', 'api/products', [], [], ['products' => $file], []);

        $this->assertSessionHasErrors();

        @unlink(public_path('files') . $name);
    }
}
