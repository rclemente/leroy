<?php

use App\Repositories\ProductRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\Product;


class ProductTest extends TestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        App\Models\Product::create([
            'lm' => '9999999',
            'name' => 'ProductFake',
            'category' => '1',
            'free_shipping' => 1,
            'description' => 'Product Fake',
            'price' => 32.50
        ]);
    }

    public function testIfProductCanBeCreated()
    {
        $product = new Product();
        $product->lm = '9999990';
        $product->name = 'ProductFake';
        $product->category = '1';
        $product->free_shipping = '1';
        $product->description = 'Product Fake';
        $product->price = 32.50;

        $this->assertTrue($product->save());
    }

    public function testIfProductCanBeUpdated()
    {
        $product = Product::find('9999999');
        $product->name = 'ProductFakeUpdated';
        $this->assertTrue($product->save());
    }

    public function testIfProductCanBeDeleted()
    {
        $product = Product::find('9999999');
        $this->assertTrue($product->delete());
    }

    public function testIfProductCanBeReaded()
    {
        $product = Product::find('9999999');
        $this->assertNotNull($product);
    }
}
