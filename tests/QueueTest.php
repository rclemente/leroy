<?php

use App\Models\Queue;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class QueueTest extends TestCase
{
    use DatabaseTransactions;

    public $id = null;
    protected function setUp()
    {
        parent::setUp();
        $queue = new Queue();
        $queue->id = 9999999;
        $queue->status = 0;
        $queue->save();
    }

    public function testIfQueueCanBeCreated()
    {
        $queue = new Queue();
        $queue->status = 0;

        $this->assertTrue($queue->save());
    }

    public function testIfQueueCanBeUpdated()
    {
        $queue = Queue::find(9999999);
        $queue->status = 1;
        $this->assertTrue($queue->save());
    }
}
