# Leroy Challenge

A web system for product management

## Requirements

- PHP 5.2 or greather
- Apache
- Mysql

## Installation

1. Clone the repository `https://gitlab.com/rclemente/leroy.git`.
2. Configure the env file for the data in your environment.
3. Open `http://localhost` in your browser.

## Documentation

The project documentation is built with Swagger and can be found at `http://localhost`