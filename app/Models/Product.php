<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 		@SWG\Definition(
 * 			@SWG\Property(property="lm", type="integer"),
 * 			@SWG\Property(property="category", type="integer"),
 * 			@SWG\Property(property="name", type="string"),
 * 			@SWG\Property(property="free_shipping", type="boolean"),
 * 			@SWG\Property(property="description", type="string"),
 * 			@SWG\Property(property="price", type="number"),
 * 		),
 */
class Product extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'lm';
    protected $fillable = [
        'lm', 'category', 'name', 'free_shipping', 'description',
        'price'
    ];

    public static function IsValid($product) {
        if(!is_array($product)) return false;
        if(!$product['lm']) return false;
        if(!$product['category']) return false;
        if(!$product['name']) return false;
        if(!$product['description']) return false;
        if(!$product['price']) return false;
        return true;
    }
    
}
