<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * 		@SWG\Definition(
 * 			@SWG\Property(property="status", type="integer"),
 * 		),
 */
class Queue extends Model
{
    protected $fillable = [
        'status', 'comments'
    ];
}
