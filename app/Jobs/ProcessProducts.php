<?php

namespace App\Jobs;

use App\Repositories\ProductRepository;
use App\Repositories\QueueRepository;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Facades\Excel;

class ProcessProducts extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;


    protected $file;
    protected $queueID;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file, $queueID)
    {
        $this->file = $file;
        $this->queueID = $queueID;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->queueID;
        try {
            QueueRepository::edit($id, 1);
            ProductRepository::process($this->file, $id);
        } catch(\Exception $exception) {
            QueueRepository::edit($id, 3, $exception->getMessage());
        }
    }

}
