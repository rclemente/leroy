<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function() {
    return redirect('api/docs');
});
Route::group(['prefix' => 'api', 'middleware' => ['api']], function () {
    Route::get('/products', 'ProductsController@read');
    Route::get('/products/{id}', 'ProductsController@read');
    Route::post('/products', 'ProductsController@create');
    Route::put('/products/{id}', 'ProductsController@update');
    Route::delete('/products/{id}', 'ProductsController@delete');
    Route::get('/queue/{id}', 'QueuesController@read');
});
