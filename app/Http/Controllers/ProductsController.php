<?php
namespace App\Http\Controllers;

use App\Jobs\ProcessProducts;
use App\Repositories\ProductRepository;
use App\Repositories\QueueRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * @SWG\Swagger(
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Documentação Leroy",
 *     ),
 *   schemes={"http"},
 *   host="localhost/api/",
 * )
 *

 */
class ProductsController extends Controller
{

    /**
     * @SWG\Get(
     *   path="/products/{id}",
     *   summary="List Products",
     *   tags={"Products"},
     *   @SWG\Response(
     *     response=200,
     *     description="A list with products"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     required=false,
     *   )
     * )
     */
    public function read($id = null)
    {
        $products = ProductRepository::read($id);
        if(!$products) {
            abort(404);
        }
        return $products;
    }

    /**
     * @SWG\Put(
     *   path="/products/{id}",
     *   summary="Update a Product",
     *   tags={"Products"},
     *   @SWG\Response(
     *     response=200,
     *     description="Returns result status"
     *   ),
     *   @SWG\Response(
     *     response=403,
     *     description="Invalid Product Requested"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="lm",
     *     in="formData",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="category",
     *     in="formData",
     *     type="integer",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="free_shipping",
     *     in="formData",
     *     type="boolean",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="description",
     *     in="formData",
     *     type="string",
     *     required=true,
     *   ),
     *   @SWG\Parameter(
     *     name="price",
     *     in="formData",
     *     type="number",
     *     required=true,
     *   ),
     * )
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'lm' => 'required',
            'category' => 'required',
            'name' => 'required',
            'price' => 'required',
        ]);
        $result = ProductRepository::edit($id, $request->input());
        if(!$result) return abort(403);
    }

    /**
     * @SWG\Delete(
     *   path="/products/{id}",
     *   summary="Delete a Product",
     *   tags={"Products"},
     *   @SWG\Response(
     *     response=200,
     *     description="Returns the result of execution"
     *   ),
     *   @SWG\Response(
     *     response=403,
     *     description="Invalid Product Requested"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     required=true,
     *   )
     * )
     */
    public function delete($id)
    {
        $result = ProductRepository::exclude($id);
        if(!$result) return abort(403);
    }

    /**
     * @SWG\Post(
     *   path="/products",
     *   summary="File upload for product inclusion",
     *   tags={"Products"},
     *   @SWG\Response(
     *     response=200,
     *     description="Returns the result of execution and the Queue id"
     *   ),
     *   @SWG\Response(
     *     response=403,
     *     description="Invalid Product Requested"
     *   ),
     *   @SWG\Parameter(
     *     name="products",
     *     in="formData",
     *     type="file",
     *     required=true,
     *   )
     * )
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'products' => 'required|mimes:xlsx, application/octet-stream,
            application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ]);

        $file = $request->file('products');
        $fileName = $file->getClientOriginalName();

        $file->move(public_path('files'), $fileName);
        $id = QueueRepository::insert();
        $this->dispatch(new ProcessProducts($fileName, $id));
        return array('Status' => 'Success', 'JobID' => $id);

    }

}
?>