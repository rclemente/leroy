<?php
namespace App\Http\Controllers;

use App\Repositories\QueueRepository;

class QueuesController extends Controller
{
    /**
     * @SWG\Get(
     *   path="/queue/{id}",
     *   summary="Displays the processing status",
     *   tags={"Queue"},
     *   @SWG\Response(
     *     response=200,
     *     description="Displays queue processing result"
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="Queue id not found"
     *   ),
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     required=true,
     *   )
     * )
     */
    public function read($id)
    {
        $queue = QueueRepository::read($id);
        if(!$queue) abort(404);
        return $queue;
    }
}
?>