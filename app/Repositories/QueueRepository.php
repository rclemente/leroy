<?php
namespace App\Repositories;

use App\Models\Queue;
use Illuminate\Database\Eloquent\Model;

class QueueRepository extends Model
{

    public static function insert()
    {
        $queue = new Queue();
        $queue->status = 0;
        $queue->save();
        return $queue->id;
    }

    public static function edit($queueID, $status, $comments = null)
    {
        $queue = Queue::find($queueID);
        if(!$queue) return false;
        $queue->status = $status;
        if($comments) {
            $queue->comments = $comments;
        }
        return $queue->save();
    }

    public static function read($queueID)
    {
        $queue = Queue::find($queueID);
        if(!$queue) return false;
        $queue->statusDescription = self::getStatus($queue->status);
        return $queue;
    }

    public static function updateComment($queueID, $comment)
    {
        $queue = Queue::find($queueID);
        $queue->comments = $comment;
        $queue->save();
    }

    private static function getStatus($statusID)
    {
        $status = ['Aguardando Processamento', 'Processando', 'Processado', 'Falha'];
        return $status[$statusID];
    }
}
?>