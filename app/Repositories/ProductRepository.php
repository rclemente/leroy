<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use League\Flysystem\Exception;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Product;

class ProductRepository extends Model
{
    public static function edit($id, $data)
    {
        $product = Product::find($id);
        if(!$product) return false;
        $product->name = isset($data['name']) ? $data['name'] : $product->name ;
        $product->category = isset($data['category']) ? $data['category'] : $product->category ;
        $product->free_shipping = isset($data['free_shipping']) ? $data['free_shipping'] : $product->free_shipping ;
        $product->description = isset($data['description']) ? $data['description'] : $product->description ;
        $product->price = isset($data['price']) ? $data['price'] : $product->price ;
        return $product->save();
    }

    public static function exclude($id)
    {
        $product = Product::find($id);
        if(!$product) {
            return false;
        }
        $product->delete();
        return true;
    }

    public static function read($id = null)
    {
        if($id) {
            $product = Product::find($id);
            if(!$product) {
                return false;
            }
            return $product;
        }
        return Product::All();
    }

    public static function insert($data)
    {
        $product = new Product();
        $product->lm = isset($data['lm']) ? $data['lm'] : $product->name ;
        $product->name = isset($data['name']) ? $data['name'] : $product->name ;
        $product->category = isset($data['category']) ? $data['category'] : $product->category ;
        $product->free_shipping = isset($data['free_shipping']) ? $data['free_shipping'] : $product->free_shipping ;
        $product->description = isset($data['description']) ? $data['description'] : $product->description ;
        $product->price = isset($data['price']) ? $data['price'] : $product->price ;
        return $product->save();
    }

    public static function process($fileName, $id, $directory = null)
    {
        if(!$directory) {
            $directory = 'public\\files\\';
        }
        Excel::selectSheetsByIndex(0)->load($directory . $fileName, function ($reader) {
            $reader->takeColumns(5);
            $header = $reader->first();
            $category = $header[1];
            if(!$category) {
                throw new Exception('Codigo de categoria invalido');
            }
            $reader->skipRows(3);
            $rows = $reader->toArray();
            if(count($rows) <= 0) {
                throw new Exception('Produtos nao encontrados');
            }
            foreach ($rows as $row) {
                if(empty($row[0]) || empty($row[1])) {
                    throw new Exception('Produtos fora do layout');
                }
                self::upsert(array(
                    'lm' => $row[0],
                    'category' => $category,
                    'name' => $row[1],
                    'free_shipping' => $row[2],
                    'description' => $row[3],
                    'price' => $row[4]
                ));
            }
        });
        QueueRepository::edit($id, 2);
    }

    public static function upsert($data)
    {
        $id = $data['lm'];
        $product = Product::find($id);
        if($product) {
            return self::edit($id, $data);
        } else {
            return self::insert($data);
        }
    }

}
?>